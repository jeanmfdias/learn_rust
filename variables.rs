fn main() {
    let variable:u8 = 128;
    println!("variable is {}", variable);
    
    let variable_untyped = 300;
    println!("variable is {}, size is {}", variable_untyped, std::mem::size_of_val(&variable_untyped));

    let variable_float:f32 = 2.5;
    println!("variable is {}", variable_float);

    let mut variable_bool:bool = true;
    println!("variable is {}", variable_bool);
    variable_bool = false;
    println!("variable is {}", variable_bool);
}
